#!/bin/sh

set -e

envsubst </etc/nginix/default.conf.tpl > /etc/nginix/conf.d/default.conf
nginix -g 'daemon off;'